//
//  OfflineSupportManager.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 22/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import UIKit
import Alamofire

class OfflineSupportManager {

    private static let sharedManager: OfflineSupportManager = {
        let sharedManager = OfflineSupportManager()
        return sharedManager
    }()
    
    class func shared() -> OfflineSupportManager {
        return sharedManager
    }
    
    func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    func saveData(_ data: Any, for key: String){
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func dataFor(key: String) -> Any? {
        return UserDefaults.standard.object(forKey: key)
    }
    
    func searchedResultsFor(key: String) -> FlickerSearchResults? {
        if let data = dataFor(key: key), let results = data as? [AnyHashable : Any]{
            return FlickerSearchResults.init(data: results)
        }
        return nil
    }
}


