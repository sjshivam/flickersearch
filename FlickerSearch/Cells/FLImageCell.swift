//
//  FLImageCell.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 21/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import UIKit
import AlamofireImage

class FLImageCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
        self.layer.cornerRadius = 4.0
    }

    func update(urlString: String){
        
        guard let url = URL(string: urlString) else{
            imageView.image = #imageLiteral(resourceName: "placeholder_loading")
            return
        }
        
        imageView.af_setImage(
            withURL: url,
            placeholderImage: #imageLiteral(resourceName: "placeholder_loading"),
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
}
