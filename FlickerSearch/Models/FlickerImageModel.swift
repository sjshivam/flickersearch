//
//  FlickerImageModel.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 21/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import UIKit

class FlickerSearchResults{
    let page: Int
    let pages : Int
    let perpage: Int
    let total: String
    private(set) var images = [FlickerImageModel]()
    
    init(data: [AnyHashable : Any]) {
        page = data["page"] as? Int ?? 1
        pages = data["pages"] as? Int ?? 1
        perpage = data["perpage"] as? Int ?? 0
        total = data["total"] as? String ?? "0"
        
        if let photos = data["photo"] as? [[String: Any]]{
            images = photos.map({ (dict) -> FlickerImageModel in return FlickerImageModel.init(data: dict) })
        }
    }
    
    func appendImages(_ images: [FlickerImageModel]){
        self.images = images + self.images
    }
    
    func toDictionary() -> [AnyHashable: Any] {
        
        let imageDict = images.map({ (model) -> [AnyHashable: Any] in return model.toDictionary() })
        
        let dict = ["page":page,
                    "pages" : pages,
                    "perpage": perpage,
                    "total" : total,
                    "photo" : imageDict
            ] as [String : Any]
        return dict
    }
}

class FlickerImageModel {

    let image_id: String
    let urlString: String
    let title: String
    
    init(data: [AnyHashable : Any]) {
        image_id = data["id"] as? String ?? ""
        urlString = data["url_m"] as? String ?? ""
        title = data["title"] as? String ?? ""
    }
    
    func toDictionary() -> [AnyHashable: Any] {
        let dict = ["id":image_id,
                    "url_m" : urlString,
                    "title": title]
        return dict
    }
}
