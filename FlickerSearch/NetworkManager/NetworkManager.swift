//
//  NetworkManager.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 21/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator

class NetworkManager {

    private static let sharedNetworkManager: NetworkManager = {
        NetworkActivityIndicatorManager.shared.isEnabled = true
        let networkManager = NetworkManager()
        return networkManager
    }()

    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    // MARK: - Properties
    private static let API_KEY = "c0c4078489a478572c549bbdb87769fe"
    
    
    // With Alamofire
    func fetchImages(keyword: String, pageNumber: Int, completion: @escaping (FlickerSearchResults?, Error?) -> Void) {
        guard let url = URL(string: "https://api.flickr.com/services/rest/") else {
            completion(nil, FLError.defaultError)
            return
        }
        
        if OfflineSupportManager.shared().isConnectedToInternet() == false{
            let result = OfflineSupportManager.shared().searchedResultsFor(key: keyword)
            if result != nil{
                completion(result, nil)
                return
            }
        }
        
        Alamofire.request(url,
                          method: .get,
                          parameters: ["method": "flickr.photos.search",
                                       "api_key" : NetworkManager.API_KEY,
                                       "text" : keyword,
                                       "per_page": 20,
                                       "extras" : "url_m",
                                       "page" : pageNumber,
                                       "format" : "json",
                                       "nojsoncallback" : 1])
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                        completion(nil, response.result.error)
                    return
                }
                
                guard let value = response.result.value as? [String: Any],
                let photoDict = value["photos"] as? [String: Any] else{
                    completion(nil, FLError.defaultError)
                    return
                }
                completion(FlickerSearchResults.init(data: photoDict), nil)
        }
    }
}

public enum FLError: Error {
    case defaultError
}

extension FLError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .defaultError:
            return NSLocalizedString("Oops! Something went wrong!!", comment: "")
        }
    }
}

