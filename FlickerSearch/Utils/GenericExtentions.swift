//
//  GenericExtentions.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 21/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView{
    
    func isFirstCell(for indexpath: IndexPath) -> Bool {
        return ((indexpath.section == 0) && (indexpath.row == 0))
    }
    
    func isLastCell(for indexpath: IndexPath) -> Bool {
        return (self.numberOfItems(inSection: indexpath.section) - 1 ) == indexpath.row
    }
}
