//
//  FLTextField.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 21/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import UIKit

enum TextFieldViewPosition {
    case left, right
}

enum TextFieldViewType {
    case loader
    case actionButton(iconName: String)
}

private struct FLTextFieldConstants
{
    static let leftViewSidePadding: CGFloat = 10
    static let rightViewSidePadding: CGFloat = 10
    
    static let textFiledViewTintColor = UIColor.black.withAlphaComponent(0.5)
    static let backgroundColor = UIColor.white
    
    static let textViewSize = CGSize(width: 25, height: 25)
}

public class FLTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.backgroundColor = FLTextFieldConstants.backgroundColor
        self.textColor = UIColor.black
    }
    
    // MARK: UITextFieldViewMode
    override public func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += FLTextFieldConstants.leftViewSidePadding
        return textRect
    }
    
    override public func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= FLTextFieldConstants.leftViewSidePadding
        return textRect
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, FLTextFieldConstants.leftViewSidePadding, 0, FLTextFieldConstants.rightViewSidePadding)))
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, FLTextFieldConstants.leftViewSidePadding, 0, FLTextFieldConstants.rightViewSidePadding)))
    }
}

extension FLTextField
{
    func setTextFiledView(viewType: TextFieldViewType?, position: TextFieldViewPosition, tapHandler: ((TextFieldViewButton)->Void)?)
    {
        if let type = viewType
        {
            var view: UIView
            
            switch type{
            case .actionButton(let iconName):
                    let button = TextFieldViewButton.init(type: .system)
                    button.frame = CGRect(x: 0, y: 0, width: FLTextFieldConstants.textViewSize.width, height: FLTextFieldConstants.textViewSize.height)
                    button.setImage(UIImage.init(named: iconName), for: .normal)
                    button.tintColor = FLTextFieldConstants.textFiledViewTintColor
                    button.tapHandler = tapHandler
                    view = button
                
            case .loader:
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                view = spinner
            }
            
            switch position {
            case .left:
                leftViewMode = UITextFieldViewMode.always
                leftView = view
                view.tag = 0
            case .right:
                rightViewMode = UITextFieldViewMode.always
                rightView = view
                view.tag = 1
            }
        }
        else
        {
            switch position {
            case .left:
                leftViewMode = UITextFieldViewMode.never
                leftView = nil
            case .right:
                rightViewMode = UITextFieldViewMode.never
                rightView = nil
            }
        }
    }
}

public class TextFieldViewButton: UIButton {
    public var tapHandler: ((TextFieldViewButton)->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    @objc func buttonTapped(_ sender: UIButton){
        tapHandler?(self)
    }
}

