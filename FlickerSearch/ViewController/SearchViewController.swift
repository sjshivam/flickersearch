//
//  SearchViewController.swift
//  FlickerSearch
//
//  Created by Shivam Jaiswal on 21/09/18.
//  Copyright © 2018 EkAnek. All rights reserved.
//

import UIKit

private struct PaddingConstants {
    static let cellInBetweenPadding: CGFloat = 10
    static let cellVerticalPadding: CGFloat = 10
    static let cellSidePadding: CGFloat = 10
    static let sectionInset: UIEdgeInsets = UIEdgeInsetsMake(0, cellSidePadding, 0, cellSidePadding)
}

class SearchViewController: UIViewController {

    @IBOutlet weak var textField: FLTextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var searchText = ""
    var pageNumber = 1
    
    var cellsPerRow = 2 {
        didSet{
            columnLayout = FLColumnFlowLayout.init(cellsPerRow: cellsPerRow, minimumInteritemSpacing: PaddingConstants.cellInBetweenPadding, minimumLineSpacing: PaddingConstants.cellVerticalPadding, sectionInset: PaddingConstants.sectionInset)
            collectionView.reloadData()
        }
    }
    
    var columnLayout = FLColumnFlowLayout.init(cellsPerRow: 2, minimumInteritemSpacing: PaddingConstants.cellInBetweenPadding, minimumLineSpacing: PaddingConstants.cellVerticalPadding, sectionInset: PaddingConstants.sectionInset){
        didSet{
            columnLayout.scrollDirection = .vertical
            collectionView.collectionViewLayout = columnLayout
        }
    }
    
    var results = FlickerSearchResults(data: [:]){
        didSet(oldValue){
            let oldImages = (pageNumber > 1) ? oldValue.images : [FlickerImageModel]()
            self.results.appendImages(oldImages)
            OfflineSupportManager.shared().saveData(self.results.toDictionary(), for: searchText)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        showOptionsButton()
        
        columnLayout.scrollDirection = .vertical
        collectionView.collectionViewLayout = columnLayout
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.delegate = self;
        collectionView.dataSource = self;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    func downloadData(){
        
        if searchText.isEmpty {
            showAlert(message: "Kindly Enter A Valid Search Keyword")
            return
        }
        
        showLoader()
        NetworkManager.shared().fetchImages(keyword: searchText, pageNumber: pageNumber) { [weak self] (results, error) in
            
            self?.showOptionsButton()
            
            guard let _self = self, let results = results else{
                self?.showAlert(message: error?.localizedDescription)
                return
            }
            
            _self.results = results
            _self.collectionView.reloadData()
        }
    }
    
    func showLoader(){
        textField.setTextFiledView(viewType: .loader, position: .right, tapHandler: nil)
    }
    
    func showOptionsButton(){
        textField.setTextFiledView(viewType: .actionButton(iconName: "options"), position: .right) { [weak self] (button) in
            self?.showColumnOptions()
        }
    }
    
    func shouldLoadNextPage(for indexPath: IndexPath) -> Bool {
       return collectionView.isLastCell(for: indexPath) && pageNumber < results.pages && OfflineSupportManager.shared().isConnectedToInternet()
    }
}

extension SearchViewController: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchText = textField.text ?? ""
        pageNumber = 1
        downloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    // MARK: collectionView
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.images.count
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FLImageCell", for: indexPath) as! FLImageCell
        let item = results.images[indexPath.row]
        cell.update(urlString: item.urlString)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if shouldLoadNextPage(for: indexPath){
            pageNumber = pageNumber + 1
            downloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(results.images[indexPath.row].image_id)
    }
}

extension SearchViewController{
   
    private func showAlert(message: String?) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Okay", style: .default) {(_) in }
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func showColumnOptions() {
        let alert = UIAlertController(title: "Select Number of Columns", message: nil, preferredStyle: .actionSheet)
        let action_2 = UIAlertAction(title: "Two", style: .default) { [weak self] (_) in
            self?.cellsPerRow = 2
        }
        alert.addAction(action_2)
        
        let action_3 = UIAlertAction(title: "Three", style: .default) { [weak self] (_) in
            self?.cellsPerRow = 3
        }
        alert.addAction(action_3)
        
        let action_4 = UIAlertAction(title: "Four", style: .default) { [weak self] (_) in
            self?.cellsPerRow = 4
        }
        alert.addAction(action_4)
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) {(_) in}
        alert.addAction(actionCancel)

        present(alert, animated: true, completion: nil)
    }
}
